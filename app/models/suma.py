from peewee import Model
from peewee import AutoField
from peewee import MySQLDatabase
from app.libraries import database_conection

class Suma(Model):
    suma_field = AutoField()
    class Meta:
        table_name = 'suma'
        database = database_conection.databaseDatos

def insert_suma(params):
    suma = {
        'suma_field': params
    }
    return (
        Suma.insert(suma)
        .execute(database_conection.databaseDatos)
    )

