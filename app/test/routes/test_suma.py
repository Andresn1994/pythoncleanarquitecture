
import sys
sys.path.append('/home/andres/pythonAWS')
import pytest
from app.routes import suma


def test_post_role(mocker):
    event = {
        'body': '{"suma_int": 10}'
    }
    response = suma.insert_suma(event, 'context')
    assert str(response['statusCode']) == '200'
