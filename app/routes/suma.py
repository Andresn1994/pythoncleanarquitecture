import json
from app.libraries import database_conection
from app.controllers import suma_manager
from app.libraries import api_rest

@database_conection.database_datos
def insert_suma(event, context):
    body = json.loads(event['body'])
    print(body)
    suma_manager.insert_suma(body)
    return api_rest.rest_response('successful')
