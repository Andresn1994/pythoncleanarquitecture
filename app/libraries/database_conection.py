from peewee import MySQLDatabase

databaseDatos = None

def database_datos(func):
    """Start conections and close after run function"""
    def wrapper(*args, **kwargs):
        try:
            event = args[0]
            connect_master(event)
            return func(*args, **kwargs)
        finally:
            close_master()
    return wrapper

def connection_database(event):
    return MySQLDatabase(
        'datos',
        user='root', 
        password='2017Nominaz_devmysql2018',
        host='localhost', 
        port=3306
    )

def connect_master(event):
    """Connect to the master database"""
    global databaseDatos
    databaseDatos = connection_database(event)

def close_master():
    """Close master database connection"""
    global databaseDatos
    databaseDatos.close()
