"""This module has rest methods"""
import json

def rest_response(reponse_data):
    """Response status 200"""
    data = {
        'response': reponse_data
    }
    return {
        'statusCode': 200,
        'body': json.dumps(data),
        'headers': {'Content-Type': 'application/json'}
    }

def response_error(reponse_data):
    """Response status 200"""
    data = {
        'message': reponse_data
    }
    return {
        'statusCode': 500,
        'body': json.dumps(data),
        'headers': {'Content-Type': 'application/json'}
    }